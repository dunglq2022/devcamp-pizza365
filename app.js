//Khai báo express
const express = require('express');

//Cấu hình Cors
const cors = require('cors');

//Khai báo port
const port = 8000;

//Tạo app
const app = express();

//Khai báo express json body
app.use(express.json());

// Cấu hình các tùy chọn cho CORS nếu cần thiết
const corsOptions = {
    origin: 'https://134.209.106.40:8000',
    methods: 'GET,HEAD,PUT,PATCH,POST,DELETE',
    credentials: true,
  };

//Khai báo CORS
app.use(cors());

//Khai báo thư viện mongoose
const mongoose = require('mongoose')

mongoose.connect("mongodb+srv://tddv2023:A7D0uI3V2hP20Ixe@pizza365.gwvuzll.mongodb.net/CRUD_Pizza365", {
    dbName: "MyPizzaApp"
}, (err) => {
    if (err) throw err;
    console.log("Connect MongoDB Successfully!");
});

//Import Router
const {drinkRouter} = require ('./app/router/drinkRouter')
const {voucherRouter} = require('./app/router/voucherRouter')
const {usersRouter} = require('./app/router/userRouter')
const {ordersRouter} = require('./app/router/orderRouter')
app.use(express.static(__dirname + '/views'))

app.get('/', (req, res) => {
    res.sendFile(__dirname + '/views/index.html');
})

app.use('/devcamp-pizza365', drinkRouter);
app.use('/devcamp-pizza365', voucherRouter);
app.use('/devcamp-pizza365', ordersRouter);
app.use('/devcamp-pizza365', usersRouter);

app.listen(port, () => {
    console.log(`Server running at 134.209.106.40:${port}/`);
})