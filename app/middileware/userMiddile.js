const getAllUsersMiddile = (req, res, next) => {
    console.log('GET ALL USERS')
    next();
}

const getOnlyUsersMiddile = (req, res, next) => {
    console.log('GET ONLY USERS')
    next();
}

const createUsersMiddile = (req, res, next) => {
    console.log('CREATE USER')
    next(); 
}

const updateUsersMiddile = (req, res, next) => {
    console.log('UPDATE USER')
    next();  
}

const deleteUsersMiddile = (req, res, next) => {
    console.log('DELETE USERS')
    next();  
}

module.exports = {
    getAllUsersMiddile,
    getOnlyUsersMiddile,
    createUsersMiddile,
    updateUsersMiddile,
    deleteUsersMiddile
}