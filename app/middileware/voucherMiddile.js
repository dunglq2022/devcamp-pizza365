const getAllVouchersMiddile = (req, res, next) => {
    console.log('GET ALL VOUCHERS')
    next();
}

const getOnlyVouchersMiddile = (req, res, next) => {
    console.log('GET ONLY VOUCHER')
    next();
}

const createVouchersMiddile = (req, res, next) => {
    console.log('CREATE VOUCHER')
    next(); 
}

const updateVouchersMiddile = (req, res, next) => {
    console.log('UPDATE VOUCHERS')
    next();  
}

const deleteVouchersMiddile = (req, res, next) => {
    console.log('DELETE VOUCHERS')
    next();  
}

module.exports = {
    getAllVouchersMiddile,
    getOnlyVouchersMiddile,
    createVouchersMiddile,
    updateVouchersMiddile,
    deleteVouchersMiddile
}