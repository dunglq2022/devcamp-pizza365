const getAllOrdersMiddile = (req, res, next) => {
    console.log('GET ALL ORDERS')
    next();
}

const getOnlyOrdersMiddile = (req, res, next) => {
    console.log('GET ONLY ORDERS')
    next();
}

const createOrdersMiddile = (req, res, next) => {
    console.log('CREATE ORDER')
    next(); 
}

const updateOrdersMiddile = (req, res, next) => {
    console.log('UPDATE ORDER')
    next();  
}

const deleteOrdersMiddile = (req, res, next) => {
    console.log('DELETE ORDERS')
    next();  
}

module.exports = {
    getAllOrdersMiddile,
    getOnlyOrdersMiddile,
    createOrdersMiddile,
    updateOrdersMiddile,
    deleteOrdersMiddile
}