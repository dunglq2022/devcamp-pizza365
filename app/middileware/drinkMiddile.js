const getAllDrinksMiddile = (req, res, next) => {
    console.log('GET ALL DRINKS')
    next();
}

const getOnlyDrinksMiddile = (req, res, next) => {
    console.log('GET ONLY DRINKS')
    next();
}

const createDrinksMiddile = (req, res, next) => {
    console.log('CREATE DRINK')
    next(); 
}

const updateDrinksMiddile = (req, res, next) => {
    console.log('UPDATE DRINK')
    next();  
}

const deleteDrinksMiddile = (req, res, next) => {
    console.log('DELETE DRINKS')
    next();  
}

module.exports = {
    getAllDrinksMiddile,
    getOnlyDrinksMiddile,
    createDrinksMiddile,
    updateDrinksMiddile,
    deleteDrinksMiddile
}