//B1 import model
const mongoose = require('mongoose')
const voucherModel = require ('../models/voucherModel')

//B2 Tạo các funtion CRUD
//Get all Voucher
const getAllVouchers = (req, res) => {
    //B1 thu thập dữ liệu
    //B2 Validate
    //B3 Xử lý nghiệp vụ
    voucherModel.find((err, data) => {
        if (err) {
            res.status(500).json({
                message: `Error find voucher ${err}`
            })
        }else {
            res.status(200).json({
                message: `Get All Vouchers`,
                data: data
            })
        }
    })
}

//Get Voucher By Id
const getVoucherById = (req, res) => {
    //B1 thu thập dữ liệu
    let maVoucher = req.params.maVoucher;
    console.log(maVoucher)
    //B2 Validate
    voucherModel.find({maVoucher: maVoucher}).exec((err,voucher) => {
        if (err) {
            res.status(500).json({
                message: `Error find voucher ${err}`
            })
        } else {
           if (voucher) {
            res.status(200).json({
                message: `Voucher tồn tại`,
                data: voucher
            })
           } else {
            res.status(200).json({
                message: `Voucher không tồn tại`
            })
           }
        }
    })
    //B3 Xử lý nghiệp vụ

}

//Get Voucher By Id
const createVoucher = (req, res) => {
    //B1 Thu thập dữ liệu
    let body = req.body;
    console.log(body);
    //B2 Validate
    if (!body.maVoucher) {
        return res.status(400).json({
            message: 'maVoucher is required'
        })
    }
    if (!body.phamTramGiamGia) {
        return res.status(400).json({
            message: 'phamTramGiamGia is required'
        })
    }

    //B3 Xử lý nghiệp vụ
    let newVoucher = {
        _id: mongoose.Types.ObjectId(),
        maVoucher: body.maVoucher,
        phamTramGiamGia: body.phamTramGiamGia,
        ghiChu: body.ghiChu
    }
    
    voucherModel.create(newVoucher, (err, data) => {
        if (err) {
            return res.status(500).json({
                message: `Error on create voucher ${err}`
            })
        } else{
            return res.status(201).json({
                message: 'Create Vouchers',
                data: data
            })
        }
    })
}


//Get Voucher By Id
const updateVoucherById = (req, res) => {
    //B1 Thu thập dữ liệu
    let voucherId = req.params.voucherId
    let body = req.body;
    //B2 Validate
    if (!mongoose.Types.ObjectId.isValid(voucherId)) {
        return res.status(400).json({
            message: 'Voucher Id is not valid'
        })
    }
    //B3 Xử lý nghiệp vụ
    let updateVoucher = new voucherModel({
        maVoucher: body.maVoucher,
        phamTramGiamGia: body.phamTramGiamGia,
        ghiChu: body.ghiChu
    })
    voucherModel.findByIdAndUpdate(voucherId, updateVoucher, (err, data) => {
        if (err) {
            return res.status(500).json({
                message: `Error on update voucher ${err}`
            })
        } else {
            return res.status(200).json({
                message: 'Update Vouchers',
                data: data
            })
        }
    })
}

//Get Voucher By Id
const deleteVoucherById = (req, res) => {
    //B1 thu thập dữ liệu
    let voucherId = req.params.voucherId
    //B2 Validate
    if (!mongoose.Types.ObjectId.isValid(voucherId)) {
        return res.status(400).json({
            message: 'Voucher Id is not valid'
        })
    }
    //B3 Xử lý nghiệp vụ
    voucherModel.findByIdAndRemove(voucherId, (err, data) => {
        if (err) {
            return res.status(500).json({
                message: `Error on delete voucher ${err}`
            })
        } else {
            return res.status(204).json({
                message: 'Delete Vouchers',
                data: data
            })
        }
    })
}

//B3 Export thành module
module.exports = {
    getAllVouchers,
    getVoucherById,
    createVoucher,
    updateVoucherById,
    deleteVoucherById
}