//Khai báo mongoose
const mongoose = require('mongoose');
//Khai báo model
const orderModel = require('../models/orderModel');
const userModel = require('../models/userModel');

//Tạo các function CRUD
//Create Order of User
const createOrder = (req, res) => {
    //B1 thu thập dử liệu
    let body = req.body;
    //B2 Kiểm tra dữ liệu
    userModel.findOne({email: body.email}).exec((err, user) => {
        if(err) {
            res.status(500).json({
                message: err,
                err
            })
        } else {
            console.log(user)
            if (user) {
                let randomOrderCode = []
                for(let i = 0; i < 6; i++) {
                    randomOrderCode.push(Math.floor(Math.random() * 10))
                }
                randomOrderCode = randomOrderCode.join('')
                let newOrder = new orderModel({
                    _id: mongoose.Types.ObjectId(),
                    orderCode: randomOrderCode,
                    pizzaSize: body.kichCo,
                    pizzaType: body.loaiPizza
                })
                orderModel.create(newOrder, (err, newOrder) => {
                    if(err) {
                        res.status(500).json({
                            message: err,
                            err
                        })
                    }  else {
                        console.log(body.email)
                        userModel.findOneAndUpdate({email:body.email}, {
                            $push: {orders: newOrder._id}
                        }, (err, userUpdate) => {
                            if (err) {
                                res.status(500).json({
                                    message: err,
                                    err
                                })
                            } else {
                                res.status(201).json({
                                    message: `New Order push on user `,
                                    userUpdate
                                })
                            }
                        })
                    }
                })
            } else {
                let newUser = new userModel({
                    _id: mongoose.Types.ObjectId(),
                    fullName: body.hoTen,
                    email: body.email,
                    address: body.diaChi,
                    phone: body.soDienThoai,
                })
                userModel.create(newUser, (err, newUser) => {
                    if(err) {
                        res.status(500).json({
                            message: err,
                            err
                        })
                    } else {
                        let randomOrderCode = []
                        for(let i = 0; i < 6; i++) {
                            randomOrderCode.push(Math.floor(Math.random() * 10))
                        }
                        randomOrderCode = randomOrderCode.join('')
                        let newOrder = new orderModel({
                            _id: mongoose.Types.ObjectId(),
                            orderCode: randomOrderCode,
                            pizzaSize: body.kichCo,
                            pizzaType: body.loaiPizza
                        })
                        orderModel.create(newOrder, (err, newOrder) => {
                            if(err) {
                                res.status(500).json({
                                    message: err,
                                    err
                                })
                            }  else {
                                console.log(body.email)
                                userModel.findOneAndUpdate({email:body.email}, {
                                    $push: {orders: newOrder._id}
                                }, (err, userUpdate) => {
                                    if (err) {
                                        res.status(500).json({
                                            message: err,
                                            err
                                        })
                                    } else {
                                        res.status(201).json({
                                            message: `New Order push on user `,
                                            userUpdate
                                        })
                                    }
                                })
                            }
                        })
                    }
                })
            }
        }
    })
}

const getAllOrder = (req, res) => {
    //B1 thu thập dữ liệu
    let userid =req.params.userid;
    //B2 Validate
    if(!mongoose.Types.ObjectId.isValid(userid)){
        return res.status(400).json({
            message: 'userId not valid!'
        })
    }
    //B3 Xử lý nghiệp vụ
    userModel.findById(userid)
    .populate('orders')
    .exec((err, data) =>{
        if (err) {
            res.status(500).json({
                message: `Error get all order`,
                err: error
            })
        } else {
            res.status(200).json({
                message: `All order by userId: ${userid}`,
                orders: data.orders
            })
        }
    })
}

const getOrderById = (req, res) => {
    let orderId = req.params.orderId;
    let userId = req.params.userId
    if(!mongoose.Types.ObjectId.isValid(orderId)){
        res.status(400).json({
            message: `UserId not valid!`
        })
    }
    if(!mongoose.Types.ObjectId.isValid(userId)){
        return res.status(400).json({
            message: 'userId not valid!'
        })
    }
    userModel.findById(userId, (err, user) => {
        if(err) {
            res.status(500).json({
                message: `Error get order by userId: ${userid}`,
                error: err.message
            })
        } else {
            orderModel.findById(orderId, (err, order) => {
                if(err) {
                    res.status(500).json({
                        message:`Error get order by id :${orderId}` ,
                        error: err.message
                    })
                } else {
                    res.status(200).json({
                        message:'Get order success',
                        order
                    })
                }
            })
        }
    })
}

const updateOrderById = (req, res) => {
    let orderId = req.params.orderId;
    let body = req.body;
    if(!mongoose.Types.ObjectId.isValid(orderId)){
        res.status(400).json({
            message: `OrderId not valid!`
        })
    }
    let updateOrder = {
        pizzaSize: body.pizzaSize,
        pizzaType: body.pizzaType,
        status: body.status
    }
    orderModel.findByIdAndUpdate(orderId, updateOrder, (err, data) => {
        if (err) {
            res.status(500).json({
                message: `Error update order by orderId: ${orderId}`,
                error: err.message
            })
        } else {
            res.status(200).json({
                message:`Update order success!`,
                order: data
            })
        }
    })
}

const deleteOrderById = (req, res) => {
    let orderId = req.params.orderId;
    if(!mongoose.Types.ObjectId.isValid(orderId)){
        res.status(400).json({
            message: `OrderId not valid!`
        })
    }
    orderModel.findByIdAndDelete(orderId, (err, data) => {
        if(err) {
            res.status(500).json({
                message: `Error order delete ${err}`,
                err: err.message
            })
        } else {
            res.status(204).json({
                message: `Delete order success!`
            })
        }
    })
}
module.exports = {
    createOrder,
    getAllOrder,
    getOrderById,
    updateOrderById,
    deleteOrderById
}