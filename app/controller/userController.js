//Khai báo thư viện mongoose
const mongoose = require ('mongoose');
//Khai báo models
const userModel =  require ('../models/userModel')
//Tạo các function CRUD

const createUser = (req, res) => {
    //B1 Thu thập dữ liệu
    let body = req.body;
    console.log(body);
    //B2 Validate
    if(!body.fullName) {
        return res.status(400).send({
            message: 'Full name is required'
        })
    }
    if(!body.email) {
        return res.status(400).send({
            message: 'email is required'
        })
    }
    if(!body.address) {
        return res.status(400).send({
            message: 'address is required'
        })
    }
    if(!body.phone) {
        return res.status(400).send({
            message: 'phone is required'
        })
    }
    //B3 Xủ lý nghiệp vụ
    let newUser = new userModel({
        _id:mongoose.Types.ObjectId(),
        fullName: body.fullName,
        email: body.email,
        address: body.address,
        phone: body.phone
    })
    userModel.create(newUser, (err, user) => {
        if (err) {
            return res.status(500).send({
                message: 'Error create user',
                error: err.message
            })
        } else {
            return res.status(201).send({
                message: 'User created',
                user: user
            })
        }
    })

}

const getAllUsers = (req, res) => {
    userModel.find({}, (err, users) => {
        if (err) {
            return res.status(500).send({
                message: 'Error get all users',
            })
        } else {
            return res.status(200).send({
                message: 'All users',
                users: users
            })
        }
    })
}

const getUserById = (req, res) => {
    //B1 thu thập dữ liệu
    let userId = req.params.userId;
    //B2 Validate
    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return res.status(400).send({
            message: 'userId is required'
        })
    }
    //B3 Xử lý nghiệp vụ
    userModel.findById(userId, (err, user) => {
        if (err) {
            return res.status(500).send({
                message: 'Error get user by id',
            })
        } else {
            return res.status(200).send({
                message: 'User by id',
                user: user
            })
        }
    })
}

const updateUserById = (req, res) => {
    //B1 thu thập dữ liệu
    let userId = req.params.userId;
    let body = req.body;
    console.log(body);
    console.log(userId);
    //B2 Validate
    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return res.status(400).send({
            message: 'userId is required'
        })
    }
    //B3 Xử lý nghiệp vụ
    let updateUser = new userModel({
        fullName: body.fullName,
        email: body.email,
        address: body.address,
        phone: body.phone
    })
    userModel.findByIdAndUpdate(userId, updateUser, (err, user) => {
        if (err) {
            return res.status(500).json({
                message: 'Error update user by id',
                err: err.message
            })
        } else {
            return res.status(200).json({
                message: 'Update user by id',
                user: user
            })
        }
    })
}

const deleteUserById = (req, res) => {
    //B1 thu thập dữ liệu
    let userId = req.params.userId;
    //B2 Validate
    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return res.status(400).json({
            message: 'userId is required'
        })
    }
    //B3 Xử lý nghiệp vụ
    userModel.findByIdAndRemove(userId, (err, user) => {
        if (err) {
            return res.status(500).json({
                message: `Error delete by id`,
                err: err.message
            })
        } else {
            return res.status(204).json({
                message: 'Delete user by id',
            })
        }
    })
}

const limitUser = (req, res) => {
    let limit = req.query.limit;
    if (isNaN(limit)|| !limit) {
        userModel.find((err, users) => {
            if(err) {
                return res.status(500).json({
                    message: 'Error limit user'
                })
            } else {
                return res.status(200).json({
                    message: 'Limit not number or not value',
                    users: users
                })
            }
        })
    } else {
        userModel.find().limit(limit).exec((err, users) => {
            if (err) {
                return res.status(500).json({
                    message: 'Error limit user',
                })
            } else {
                return res.status(200).json({
                    message: 'Limit user',
                    users: users
                })
            }
        });
    }
}

const skipUser = (req, res) => {
    let skip = req.query.skip;
    if (isNaN(skip)|| !skip) {
        userModel.find((err, users) => {
            if(err) {
                return res.status(500).json({
                    message: 'Error Skip user'
                })
            } else {
                return res.status(200).json({
                    message: 'Skip not number or not value',
                    users: users
                })
            }
        })
    } else {
        userModel.find().skip(skip).exec((err, users) => {
            if (err) {
                return res.status(500).json({
                    message: 'Error Skip user',
                })
            } else {
                return res.status(200).json({
                    message: 'Skip user',
                    users: users
                })
            }
        });
    }
}

const sortUser = (req, res) => {
    userModel.find().sort({fullName: 'asc'}).exec((err, users) => {
        if(err) {
            return res.status(500).json({
                message: 'Error sort user'
            })
        } else {
            return res.status(200).json({
                message: 'Sort Users',
                users: users
            })
        }
    })
}

const skipLimitUser = (req, res) => {
    let skip = req.query.skip;
    let limit = req.query.limit;
    console.log(skip, limit)
    if (isNaN(skip) || !skip ||isNaN(limit) || !limit) {
        userModel.find((err, users) => {
            if(err) {
                return res.status(500).json({
                    message: 'Error Skip user'
                })
            } else {
                return res.status(200).json({
                    message: 'Skip not number or not value',
                    users: users
                })
            }
        })
    } else {
        userModel.find().skip(skip).limit(limit).exec((err, users) => {
            if (err) {
                return res.status(500).json({
                    message: 'Error Skip user',
                })
            } else {
                return res.status(200).json({
                    message: `Skip: ${skip} Limit: ${limit} user`,
                    users: users
                })
            }
        }); 
    }

}
module.exports = {
    createUser,
    getAllUsers,
    getUserById,
    updateUserById,
    deleteUserById,
    limitUser,
    skipUser,
    sortUser,
    skipLimitUser
}