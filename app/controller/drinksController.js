const mongoose = require('mongoose');
const drinksModel = require('../models/drinkModel')
//Tao function CRUD
const createDrinks = (req, res) => {
    let body = req.body;
    if(!body.maNuocUong) {
        return res.status(400).json({
            message: `maNuocUong is require`
        })
    }
    if(!body.tenNuocUong) {
        return res.status(400).json({
            message: `tenNuocUong is require`
        })
    }
    if(!body.donGia) {
        return res.status(400).json({
            message: `donGia is require`
        })
    }
    let newDrink =  new drinksModel({
        _id: mongoose.Types.ObjectId(),
        maNuocUong: body.maNuocUong,
        tenNuocUong: body.tenNuocUong,
        donGia: body.donGia,
        ghiChu: body.ghiChu
    })
    newDrink.save((err, data) => {
        if(err) {
            return res.status(500).json({
                message: err
            })
        } else {
            return res.status(201).json({
                message: `Create drink success!`,
                drinks: data
            })
        }
    })
}

const getAllDrinks = (req, res) => {
    drinksModel.find({}, (err, data) => {
        if(err) {
            return res.status(500).json({
                message: err
            })
        } else {
            return res.status(200).json({
                message: `Get all drinks success!`,
                message: data
            })
        }
    })
}
module.exports = {
    createDrinks,
    getAllDrinks
}