const mongoose =  require('mongoose');
const Schema =  mongoose.Schema;
const orderSchema = new Schema ({
    _id: mongoose.Types.ObjectId,
    orderCode: {
        type: String,
        unique: true
    },
    pizzaSize: {
        type: String,
        require: true
    },
    pizzaType: {
        type: String,
        require: true
    },
    voucher: {
        type: mongoose.Types.ObjectId,
        ref: 'voucher'
    },
    status: {
        type: String,
        require: true
    }
})
module.exports = mongoose.model('order', orderSchema)