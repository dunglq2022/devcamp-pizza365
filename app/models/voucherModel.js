const mongoose =  require('mongoose');
const Schema =  mongoose.Schema;
const voucherSchema = new Schema({
    _id: mongoose.Types.ObjectId,
    maVoucher: {
        type: Number,
        required: true,
        unique: true
    },
    phamTramGiamGia: {
        type: Number,
        required: true,
    },
    ghiChu: {
        type: String,
        required: false,
    }
})
module.exports = mongoose.model('voucher', voucherSchema)