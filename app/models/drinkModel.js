const mongoose = require ('mongoose')
const Schema = mongoose.Schema;
const drinkSchema = new Schema({
    _id: mongoose.Types.ObjectId,
    maNuocUong: {
        type: String,
        required: true,
        unique: true
    },
    tenNuocUong: {
        type: String,
        required: true,
    },
    donGia: {
        type: Number,
        required: true,
    },
    ghiChu: {
        type: String,
        default: null
    }
})
module.exports = mongoose.model('drink', drinkSchema)