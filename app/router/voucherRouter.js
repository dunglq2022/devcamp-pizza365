//Khai báo express
const express = require ('express');

//Khai báo middileware
const {
    getAllVouchersMiddile,
    getOnlyVouchersMiddile,
    createVouchersMiddile,
    updateVouchersMiddile,
    deleteVouchersMiddile
} = require ('../middileware/voucherMiddile')

//Khai báo các controller
const {
    getAllVouchers,
    getVoucherById,
    createVoucher,
    updateVoucherById,
    deleteVoucherById
} = require ('../controller/voucherController')

//Khai báo router
const voucherRouter = express.Router();

voucherRouter.get('/vouchers', 
getAllVouchersMiddile, 
getAllVouchers
)

voucherRouter.get('/vouchers/:maVoucher', getOnlyVouchersMiddile, 
getVoucherById
)

voucherRouter.post('/vouchers', 
createVouchersMiddile,
createVoucher
)

voucherRouter.put('/vouchers/:voucherId',
updateVouchersMiddile, 
updateVoucherById
)

voucherRouter.delete('/vouchers/:voucherId',
deleteVouchersMiddile,
deleteVoucherById
)

module.exports = { voucherRouter };