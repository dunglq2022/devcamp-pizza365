//Khai báo express
const express = require ('express');
const drinkRouter = express.Router();
//Khai báo middileware
const {
    getAllDrinksMiddile,
    getOnlyDrinksMiddile,
    createDrinksMiddile,
    updateDrinksMiddile,
    deleteDrinksMiddile
} = require ('../middileware/drinkMiddile')

const {
    createDrinks,
    getAllDrinks
} = require('../controller/drinksController')

//Tao router
drinkRouter.post('/drinks', createDrinksMiddile, createDrinks)
drinkRouter.get('/drinks', getAllDrinksMiddile, getAllDrinks)

module.exports = { drinkRouter };