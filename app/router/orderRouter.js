//Khai báo express
const express = require ('express');
//Khai báo router
const ordersRouter = express.Router();

//Khai báo middileware
const {
    getAllOrdersMiddile,
    getOnlyOrdersMiddile,
    createOrdersMiddile,
    updateOrdersMiddile,
    deleteOrdersMiddile
} = require ('../middileware/orderMiddile')

//Khai báo controller
const {
    createOrder,
    getAllOrder,
    getOrderById,
    updateOrderById,
    deleteOrderById
} = require('../controller/orderController')

ordersRouter.post('/orders/', createOrdersMiddile, createOrder)
ordersRouter.get('/orders/:userid', getAllOrdersMiddile, getAllOrder)
ordersRouter.get('/orders/:userId/:orderId',getOnlyOrdersMiddile, getOrderById)

module.exports = { ordersRouter };