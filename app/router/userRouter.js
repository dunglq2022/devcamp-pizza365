//Khai báo express
const express = require ('express');

//Khai báo middileware
const {
    getAllUsersMiddile,
    getOnlyUsersMiddile,
    createUsersMiddile,
    updateUsersMiddile,
    deleteUsersMiddile,
} = require ('../middileware/userMiddile')

//Khai báo các controller
const {
    createUser,
    getAllUsers,
    getUserById,
    updateUserById,
    deleteUserById,
    limitUser,
    skipUser,
    sortUser,
    skipLimitUser
} = require ('../controller/userController')

//Khai báo router
const usersRouter = express.Router();

usersRouter.get('/users', getAllUsersMiddile, getAllUsers)

usersRouter.get('/users/:userId', getOnlyUsersMiddile, getUserById)

usersRouter.post('/users', createUsersMiddile, createUser )

usersRouter.put('/users/:userId', updateUsersMiddile, updateUserById)

usersRouter.delete('/users/:userId', deleteUsersMiddile, deleteUserById)

usersRouter.get('/limit-users/',getAllUsersMiddile, limitUser)

usersRouter.get('/skip-users', getAllUsersMiddile, skipUser)

usersRouter.get('/sort-users', getAllUsersMiddile, sortUser)

usersRouter.get('/skip-limit-users', getAllUsersMiddile, skipLimitUser)

module.exports = { usersRouter };